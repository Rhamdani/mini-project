import { HttpErrorResponse } from '@angular/common/http';
import { Component, Injectable, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subject } from 'rxjs';
import { Bank } from 'src/app/model/bank';
import { BankService } from 'src/app/services/bank.service';

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.css']
})

@Injectable()
export class BankComponent implements OnInit, OnDestroy {
  public bank: Bank[] = [];
  public editBank: Bank;
  public deleteBank: Bank;

  // nameBank:any;
  // p:number = 1;

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  constructor(private bankService: BankService) { 
    this.editBank = {} as Bank;
    this.deleteBank = {} as Bank;
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  ngOnInit(): void {
    this.dtOptions = {
      retrieve: true,
      lengthMenu: [3, 6, 9],
      processing: true
    };
    this.getBank();
  }

  // Search(){
  //   if(this.nameBank == ""){
  //    this.getBank();
  //   }else{
  //     this.bank = this.bank.filter(res =>{
  //       return res.nameBank.toLocaleLowerCase().match(this.nameBank.toLocaleLowerCase());
  //     });
  //   }
  // }

  // key: string = 'nameBank';
  // reverse:boolean = false;
  // sort(key: string){
  //   this.key= key;
  //   this.reverse = this.reverse;
  // }

    public getBank(): void {
    this.bankService.getBank().subscribe(
    (response: Bank[]) => {
      this.bank = response;
      this.dtTrigger.next();
    },
    (error: HttpErrorResponse) => {
      alert(error.message);
    }
    );
  }

  public onAddBank(addForm: NgForm): void {
    document.getElementById('add-bank-form');
    this.bankService.addBank(addForm.value).subscribe(
      (response: Bank) => {
        console.log(response);
        this.getBank();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    )
  }

  public onEditBank(bank: Bank): void {
    document.getElementById('edit-bank-form');
    this.bankService.editBank(bank).subscribe(
      (response: Bank) => {
        console.log(response);
        this.getBank();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public onDeleteBank(id: number): void {
    document.getElementById('delete-bank-form');
    this.bankService.deleteBank(id).subscribe(
      (response: void) => {
        console.log(response);
        this.getBank();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message)
        alert(error.message);
      }
    )
  }

  public onOpenModal(bank: Bank, mode: string): void {
    const container = document.getElementById('main-container')
    const button = document.createElement('button')
    button.type = 'none';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');

    if(mode == 'add') {
      console.log('button')
      this.getBank();
      button.setAttribute('data-target', '#addBankModal');
    }

    if(mode == 'edit') {
      this.editBank = bank;
      button.setAttribute('data-target', '#editBankModal');
    }

    if(mode == 'delete') {
      this.deleteBank = bank;
      button.setAttribute('data-target', '#deleteBankModal');
    }

    container!.appendChild(button);
    button.click();
  }
}