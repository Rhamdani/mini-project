import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-topnav',
  templateUrl: './topnav.component.html',
  styleUrls: ['./topnav.component.css']
})
export class TopnavComponent implements OnInit {

  token: String = '';

  constructor(private route: Router) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('token')!!;
  }

  public onLogout() {
    localStorage.removeItem('token');
    this.route.navigate(['/login'])
  }
}
