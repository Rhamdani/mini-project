import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BankComponent } from "../pages/bank/bank.component";
import { HomeComponent } from "../pages/home/home.component";
import { LoginComponent } from "../pages/login/login.component";


const route: Routes = [
    {path: '', redirectTo: 'login', pathMatch: 'full'},
    {path: 'login', component: LoginComponent},
    {path: 'home', component: HomeComponent},
    {path: 'bank', component: BankComponent},
]

@NgModule({
    imports: [
        RouterModule.forRoot(route, {
            initialNavigation: 'enabled',
        })
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {}