export interface Bank {
    id: number;
    nameBank: String;
    vaCode: String;
    createdBy: number;
    createdOn: Date;
    modifiedBy: number;
    modifiedOn: Date;
    deletedBy: number;
    deleted: Boolean;
}