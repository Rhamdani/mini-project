import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from '../base/config';
import { Bank } from '../model/bank';

@Injectable({
  providedIn: 'root'
})
export class BankService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      Authorization: `Bearer ${localStorage.getItem('token')}`,
    })
  }

  public getBank(): Observable<Bank[]> {
    return this.http.get<Bank[]> (
      `${Config.url}/api/bank`,
      this.httpOptions
    )
  }

  public getBankByName(name: String): Observable<Bank> {
    return this.http.post<Bank> (
      `${Config.url}/api/bank/${name}`,
      this.httpOptions
    );
  }

  public addBank(bank: Bank): Observable<Bank> {
    return this.http.post<Bank>(
      Config.url + '/api/bank',
      bank, {
        ...this.httpOptions,
        responseType: 'text' as 'json',
      }
    )
  }

  public editBank(bank: Bank): Observable<Bank> {
    return this.http.put<Bank>(
      Config.url + '/api/bank',
      bank, {
        ...this.httpOptions,
        responseType: 'text' as 'json',
      }
    )
  }

  public deleteBank(id: number): Observable<void> {
    return this.http.delete<void> (
      `${Config.url}/api/bank/${id}`, {
        ...this.httpOptions,
        responseType: 'text' as 'json',
      }
    )
  }

}
