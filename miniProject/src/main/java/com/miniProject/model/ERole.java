package com.miniProject.model;

public enum ERole {
	ROLE_DOKTER,
	ROLE_PASIEN,
	ROLE_ADMIN
}