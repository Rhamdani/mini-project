package com.miniProject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "payment")
@SQLDelete(sql = "UPDATE payment SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class PaymentMethode extends BaseProperties<Long> {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false)
	private Long id;

	@Column(length = 50)
	private String nameMethode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNameMethode() {
		return nameMethode;
	}

	public void setNameMethode(String nameMethode) {
		this.nameMethode = nameMethode;
	}

}