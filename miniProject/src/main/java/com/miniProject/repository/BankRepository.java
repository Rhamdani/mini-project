package com.miniProject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.miniProject.model.Bank;

@Repository
public interface BankRepository extends JpaRepository<Bank, Long> {
	List<Bank> findBynameBank(String name);
}

