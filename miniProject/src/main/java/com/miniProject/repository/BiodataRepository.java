package com.miniProject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.miniProject.model.Biodata;

@Repository
public interface BiodataRepository extends JpaRepository<Biodata, Long> {

}
