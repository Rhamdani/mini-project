package com.miniProject.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniProject.model.PaymentMethode;
import com.miniProject.repository.PaymentMethodeRepository;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/")
public class ApiPaymentMethodeController {

	@Autowired
	private PaymentMethodeRepository paymentMethodeRepository;

	@GetMapping("paymentmethode")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<PaymentMethode>> getAllPaymentMethode() {
		try {
			List<PaymentMethode> paymentMethode = this.paymentMethodeRepository.findAll();
			return new ResponseEntity<>(paymentMethode, HttpStatus.OK);
		} catch (Exception err) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("paymentmethode/{name}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<PaymentMethode>> getPaymentMethodeByName(@PathVariable("name") String name) {
		try {
			List<PaymentMethode> paymentMethode = this.paymentMethodeRepository.findByNameMethode(name);
			return new ResponseEntity<>(paymentMethode, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	

	@PostMapping("paymentmethode")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> savePaymentMethode(@RequestBody PaymentMethode payment) {
		PaymentMethode paymentData = this.paymentMethodeRepository.save(payment);
		if (paymentData.equals(payment)) {
			return new ResponseEntity<>("Save Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("paymentmethode")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> udpatePayment(@RequestBody PaymentMethode payment) {
		Long id = payment.getId();
		Optional<PaymentMethode> paymentData = this.paymentMethodeRepository.findById(id);
		if (paymentData.isPresent()) {
			payment.setId(id);
			this.paymentMethodeRepository.save(payment);
			return new ResponseEntity<>("Update Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@DeleteMapping("paymentmethode/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public void delete(@PathVariable Long id) {
	    paymentMethodeRepository.deleteById(id);
	}
}