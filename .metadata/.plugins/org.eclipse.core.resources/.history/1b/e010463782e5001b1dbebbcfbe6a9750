package com.miniProject.controller;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniProject.model.Bank;
import com.miniProject.repository.BankRepository;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/")
public class ApiBankController {

	@Autowired
	private BankRepository bankRepository;
	
//	@Autowired
//	private UserRepository userRepository;

	@GetMapping("bank")
	public ResponseEntity<List<Bank>> getAllBank() {
		try {
			List<Bank> bank = this.bankRepository.findAll();
			return new ResponseEntity<>(bank, HttpStatus.OK);
		} catch (Exception err) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("bank/{name}")
	public ResponseEntity<List<Bank>> getBankByName(@PathVariable("name") String name) {
		try {
			List<Bank> bank = this.bankRepository.findBynameBank(name);
			return new ResponseEntity<>(bank, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	public static Date convert(LocalDate localDate) {
		return Date.valueOf(localDate);
	}

	@PostMapping("bank")
	public ResponseEntity<Object> saveBank(@RequestBody Bank bank) {
		LocalDate todayLocalDate = LocalDate.now(
				ZoneId.of("Asia/Jakarta"));
		Date sqlDate = convert(todayLocalDate);
		bank.setCreatedOn(sqlDate);
		Bank bankData = this.bankRepository.save(bank);
		if (bankData.equals(bank)) {
			return new ResponseEntity<>("Save Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("bank")
	public ResponseEntity<Object> udpateBank(@RequestBody Bank bank) {
		LocalDate todayLocalDate = LocalDate.now(
				ZoneId.of("Asia/Jakarta"));
		Date sqlDate = convert(todayLocalDate);
		bank.setModifiedOn(sqlDate);
		Long id = bank.getId();
		Optional<Bank> bankData = this.bankRepository.findById(id);
		if (bankData.isPresent()) {
			bank.setId(id);
			this.bankRepository.save(bank);
			return new ResponseEntity<>("Update Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@DeleteMapping("bank/{id}")
	public void delete(@PathVariable Long id) {
	    bankRepository.deleteById(id);
	}

}